import os
import sys
import time

from watchdog.observers import Observer
from watchdog.events import RegexMatchingEventHandler

from xml.etree import ElementTree as xml_tree

from svg_to_gcode.svg_parser import parse_root, Transformation, debug_methods
from svg_to_gcode.geometry import LineSegmentChain
from svg_to_gcode.compiler import Compiler, interfaces
from svg_to_gcode.formulas import linear_map
from svg_to_gcode import TOLERANCES
from svg_to_gcode.svg_parser import parse_file

MACHINE_ORIGIN = "top-left"
HORIZONTAL_OFFSET = 0
VERTICAL_OFFSET = 0
INVERT_Y_AXIS = False
TRAVEL_SPEED = 3000
LASER_SPEED = 750
LASER_POWER = 1000
LASER_POWER_RANGE = "1000"
LASER_POWER_COMMAND = "M03"
LASER_OFF_COMMAND = "M05"
PASSES = 1
PASS_DEPTH = 1.0
UNIT = "mm"
DWELL_TIME = 0
BED_HEIGHT = 200
BED_WIDTH = 200
SCALING_FACTOR = 10
APPROXIMATION_TOLERANCE = "1.0"
######
REHOME_AT_START = True
REHOME_AT_END = True
PAUSE_AFTER_PENUP_IN_SECONDS = "01"
REHOME_COMMAND = "M05;\nG1 F3000 X0.0 Y0.0;"

if len(PAUSE_AFTER_PENUP_IN_SECONDS) > 0:
    LASER_OFF_COMMAND = f"{LASER_OFF_COMMAND};\nG4 P{PAUSE_AFTER_PENUP_IN_SECONDS}"


def generate_custom_interface(laser_off_command, laser_power_command, laser_power_range):
    """Wrapper function for generating a Gcode interface with a custom laser power command"""

    class CustomInterface(interfaces.Gcode):
        """A Gcode interface with a custom laser power command"""

        def __init__(self):
            super().__init__()

        def laser_off(self):
            return f"{laser_off_command};"

        def set_laser_power(self, power):
            if power < 0 or power > 1:
                raise ValueError(f"{power} is out of bounds. Laser power must be given between 0 and 1. "
                                 f"The interface will scale it correctly.")

            return f"{laser_power_command} S{linear_map(0, int(laser_power_range), power)};"

    return CustomInterface


def convert(src_path):

    filename, ext = os.path.splitext(src_path)
    svg_file = f"{filename}{ext}"
    gcode_file = f"{filename}.gcode"
    header = None
    footer = None

    if(REHOME_AT_START):
        header = [REHOME_COMMAND]

    if(REHOME_AT_END):
        footer = [REHOME_COMMAND]

    print(f"Converting: {svg_file}")

    approximation_tolerance = float(
        APPROXIMATION_TOLERANCE.replace(',', '.'))
    TOLERANCES["approximation"] = approximation_tolerance
    custom_interface = generate_custom_interface(
        LASER_OFF_COMMAND,
        LASER_POWER_COMMAND,
        LASER_POWER_RANGE)

    gcode_compiler = Compiler(
        custom_interface,
        TRAVEL_SPEED,
        LASER_SPEED,
        PASS_DEPTH,
        dwell_time=DWELL_TIME,
        custom_header=header,
        custom_footer=footer,
        unit=UNIT)

    transformation = Transformation()

    transformation.add_translation(
        HORIZONTAL_OFFSET,
        VERTICAL_OFFSET)
    transformation.add_scale(SCALING_FACTOR)
    if MACHINE_ORIGIN == "center":
        transformation.add_translation(-BED_WIDTH / 2,
                                       BED_HEIGHT / 2)

    transform_origin = True
    if MACHINE_ORIGIN == "top-left":
        transform_origin = False

    curves = parse_file(svg_file,
                        transform_origin=not INVERT_Y_AXIS,
                        canvas_height=BED_HEIGHT)
    gcode_compiler.append_curves(curves)
    gcode_compiler.compile_to_file(gcode_file, passes=PASSES)
    print(f"...Done!: {gcode_file}")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        src_path = sys.argv[1]
    else:
        print("Input file required!")
        sys.exit(0)

    print(f"WATCHING: {src_path}")
    convert(src_path)
