# GCODE Tools for Drawbot

☕ &nbsp; _Is this project helpful to you? [Please consider buying me a coffee](https://pay.feralresearch.org/tip)._

Various scripts for working with the [LY Drawbot](https://www.aliexpress.com/item/4000859943737.html). Here's a [blogpost about why](https://feralresearch.org/robot-invasion-working-with-gcode/) I did this. I also have a general writeup about this plotter including [links to software](https://feralresearch.org/the-plot-thickens/) on my blog.

This code is quickly written and I'm not much of a Python programmer - feel free to open a PR if you have better ideas - but thought this might be useful to someone else.

Happy Plotting!

## Convert

Officially you should use the [JTech Photonics Extension](https://github.com/JTechPhotonics/J-Tech-Photonics-Laser-Tool/tree/master) for [Inkscape](https://inkscape.org/). The JTech extension is open source and written in Python, so I modified it slightly to run as a command line utility:

`python convert.py INPUT.SVG`

## Watch

A wrapper for convert, watches a directory for changes. If a SVG file appears, it will automatically convert it to GCODE. If a file is delted from the watched directory it will also delete the .gcode file.

`python watch.py INPUT_DIRECTORY`

## Additional Options

Look in the converty.py file for a number of options you can set. Most of these are lifted directly from the extension, I've added three to rehome the printer at the start and the end of the run and to pause after lifting the pen, to prefent "tails" from appearing on the drawing.

```
REHOME_AT_START = True
REHOME_AT_END = True
PAUSE_AFTER_PENUP_IN_SECONDS = "01"
```

## Default Options

These are lifted from the JTech defaults, and they work pretty well for me. YMMV.

```
MACHINE_ORIGIN = "top-left"
HORIZONTAL_OFFSET = 0
VERTICAL_OFFSET = 0
INVERT_Y_AXIS = False
TRAVEL_SPEED = 3000
LASER_SPEED = 750
LASER_POWER = 1000
LASER_POWER_RANGE = "1000"
LASER_POWER_COMMAND = "M03"
LASER_OFF_COMMAND = "M05"
PASSES = 1
PASS_DEPTH = 1.0
UNIT = "mm"
DWELL_TIME = 0
BED_HEIGHT = 200
BED_WIDTH = 200
SCALING_FACTOR = 10
APPROXIMATION_TOLERANCE = "1.0"
```
